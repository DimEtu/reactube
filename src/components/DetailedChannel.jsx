import React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import {useHistory} from "react-router-dom";

const DetailedChannel = ({id, snippet, statistics}) => {

    const {title, description, publishedAt} = snippet;
    const {viewCount, subscriberCount, videoCount} = statistics;
    const history = useHistory();

    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Button variant="danger" style={{float: 'right'}} onClick={history.goBack}>Retour</Button>
            <Row>
                <Col>
                    <h2>{title}</h2>
                    <h5 className="mb-2 text-info">{title}</h5>
                    <h5 className="mb-2 text-muted">
                        {viewCount}️
                        - {subscriberCount}
                        - {videoCount}
                    </h5>
                    <Card.Text>{description}</Card.Text>
                    <Card.Text className="mb-1 text-muted">{publishedAt}</Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>);
}

export default DetailedChannel;
